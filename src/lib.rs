extern crate reqwest;
extern crate rand;

use rand::{thread_rng, seq::SliceRandom};

//use serde::{Deserialize, Serialize};

//#[derive(Debug, PartialEq, Serialize, Deserialize)]
//struct Dictionary {
//    known: Vec<String>,
//}

//TODO: make more generic
pub fn get_zhn<OnConoicalFound, OnSentencesFound>(
    on_conoical_found: &OnConoicalFound,
    on_senctences_found: &OnSentencesFound,
) -> Option<(String, String)>
where
    OnConoicalFound: Fn(&str),
    OnSentencesFound: Fn(&usize),
{
    const ADDRESS_ZHN: &str =
        "https://zh.wikipedia.org/wiki/Special:%E9%9A%8F%E6%9C%BA%E9%A1%B5%E9%9D%A2";

    // TODO: move to YAML
    //let dictonary: Dictionary = serde_yaml::from_str()?;
    let dictonary = vec![
        "人", "我", "你", "们", "多", "天", "山", "火", "车", "一", "二", "三", "男",
        "女", "海", "大", "小", "爱", "很", "吗", "上", "下",
    ];

    loop {
        let html_string = match reqwest::get(ADDRESS_ZHN) {
            Ok(mut res) => Some(String::from(res.text().unwrap())),
            _ => None,
        };

        match html_string {
            Some(html) => {
                match get_canonical(&html) {
                    Some(cononical) => on_conoical_found(&cononical),
                    None => {}
                }

                let sentences = get_sentences("。", &html);

                if !sentences.is_empty() {
                    on_senctences_found(&sentences.len());

                    let mut shuffled_dictonary = dictonary.clone();
                    shuffled_dictonary.shuffle(&mut thread_rng());
                    
                    for known in &shuffled_dictonary {
                        match find_usage(known, &sentences) {
                            Some(usage) => return Some((known.to_string(), usage)),
                            None => {}
                        }
                    }
                }
            }

            _ => return None,
        }
    }
}

pub fn get_canonical(html: &str) -> Option<String> {
    get_inner(r#"<link rel="canonical" href=""#, r#""/>"#,html)
}

pub fn get_paragraphs(html: &str) -> Vec<String> {
    let mut cleaned = Vec::new();

    for para in get_all_inner("<p>", "</p>", html) {
        if para.len() > 1 {
            cleaned.push(para.trim().to_string());
        }
    }

   cleaned
}

pub fn get_sentences(sentence_end: &str, html: &str) -> Vec<String> {
    let paragraphs = get_paragraphs(html);
    let mut sentences = Vec::new();

    for paragraph in paragraphs {
        for sentence in paragraph.split(sentence_end) {
            if sentence.len() > 1 {
                sentences.push(format!("{}{}", clean_string(sentence), sentence_end));
            }
        }
    }

    sentences
}

pub fn clean_string(tagged_string: &str) -> String {
    let mut do_build_string = true;
    let mut string_builder = String::new();

    let trimmed = tagged_string.trim();
    for character in trimmed.chars() {
        do_build_string = match character {
            '<' => false,
            '>' => true,
            '\n' | '\r' => do_build_string,
            _ => {
                if do_build_string {
                    string_builder.push(character);
                }
                do_build_string
            }
        };
    }

    string_builder
}

pub fn find_usage(target: &str, sentences: &Vec<String>) -> Option<String> {
    for sentence in sentences {
        let sentence_lower = sentence.to_ascii_lowercase();
        match sentence_lower.find(&target.to_ascii_lowercase()) {
            None => continue,
            _ => return Some(sentence.clone()),
        };
    }

    None
}

fn get_inner(from: &str, to: &str, string: &str) -> Option<String> {
    let left = match string.find(from) {
        Some(index) => Some(string.split_at(index + from.len()).1),
        None => None,
    };

    match left {
        Some(left) => match left.find(to) {
                Some(index) => Some(left.split_at(index).0.to_string()),
                None => None,
            },
        None => None
    }
}

fn get_all_inner(from: &str, to: &str, string: &str) -> Vec<String> {
    let mut return_vec = Vec::new();
    let mut workable_string = String::from(string);

    loop {
        let left_index = match workable_string.find(from) {
            Some(index) => index + from.len(),
            None => return return_vec,
        };

        let right_string = workable_string.split_at(left_index).1;
        let right_index = match right_string.find(to) {
            Some(index) => index,
            None => return return_vec,
        };

        let (inner, new_right) = right_string.split_at(right_index);
        return_vec.push(inner.to_string());

        workable_string = new_right.to_string();
    }
}

#[test]
fn test_get_inner() {
    assert_eq!(get_inner("<<<", ">>>", "<<<hello>>>"), Some("hello".to_string()));
}

#[test]
fn test_get_all_inner() {
    assert_eq!(get_all_inner("<p>", "</p>", "<p>fourteen</p><p>two</p><t>this</t>"), ["fourteen", "two"]);
}