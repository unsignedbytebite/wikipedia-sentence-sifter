extern crate wikipedia_sentence_sifter;

fn main() {
    match wikipedia_sentence_sifter::get_zhn(
        &|canonical: &str| {
            println!("Sifting wikipedia for a sentence...{}", canonical);
        },
        &|_sentences: &usize| {},
    ) {
        Some((known, usage)) =>  println!("[{}] -> {}", known, usage),
        None => println!("No connection!"),
    }
}
