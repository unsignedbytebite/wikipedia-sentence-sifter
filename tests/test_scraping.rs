extern crate wikipedia_sentence_sifter;

use std::{fs::File, io::prelude::*};

fn test_helper_load_file(path: &str) -> Option<String> {
    match File::open(path) {
        Ok(mut file) => {
            let mut contents = String::new();
            file.read_to_string(&mut contents)
                .expect("Cannot read_string");
            Some(contents.clone())
        }
        _ => None,
    }
}

#[test]
fn scrape_empty() {
    assert_eq!(test_helper_load_file("nooope_chuck_testa"), None);
}

#[test]
fn scrape_canonical_eng() {
    let html_string = test_helper_load_file("./tests/html_example_eng.html");
    assert_ne!(html_string, None);
    let canonical = wikipedia_sentence_sifter::get_canonical(&html_string.unwrap());

    assert_ne!(canonical, None);
    assert_eq!(
        canonical,
        Some("https://en.wikipedia.org/wiki/Hecke_algebra_of_a_pair".to_string())
    );
}

#[test]
fn scrape_canonical_zhn() {
    let html_string = test_helper_load_file("./tests/html_example_zhn.html");
    assert_ne!(html_string, None);
    let canonical = wikipedia_sentence_sifter::get_canonical(&html_string.unwrap());

    assert_ne!(canonical, None);
    assert_eq!(
        canonical,
        Some("https://zh.wikipedia.org/wiki/%E8%BB%8D%E7%87%9F%E6%9D%91".to_string())
    );
}

#[test]
fn scrape_paragraphs_eng() {
    let html_string = test_helper_load_file("./tests/html_example_eng.html");
    assert_ne!(html_string, None);
    let paragraphs = wikipedia_sentence_sifter::get_paragraphs(&html_string.unwrap());
    assert_eq!(paragraphs.len(), 2);
    assert_eq!(paragraphs[0], r#"In mathematical representation theory, the <b>HT</b> of a pair (<i>g</i>,<i>K</i>) is an algebra with an <a href="/wiki/Approximate_identity" title="Approximate identity">approximate identity</a>, whose approximately unital modules are the same as <i>K</i>-finite representations of the pairs (<i>g</i>,<i>K</i>). Here <i>K</i> is a compact subgroup of a <a href="/wiki/Lie_group" title="Lie group">Lie group</a> with <a href="/wiki/Lie_algebra" title="Lie algebra">Lie algebra</a> <i>g</i>."#);
    assert_eq!(paragraphs[1], r#"The Hecke algebra of a pair (<i>g</i>,<i>K</i>) is the algebra of <i>K</i>-finite distributions on <i>G</i> with support in <i>K</i>, with the product given by <a href="/wiki/Convolution" title="Convolution">convolution</a>."#);
}

#[test]
fn scrape_paragraphs_zhn() {
    let html_string = test_helper_load_file("./tests/html_example_zhn.html");
    assert_ne!(html_string, None);
    let paragraphs = wikipedia_sentence_sifter::get_paragraphs(&html_string.unwrap());
    assert_eq!(paragraphs.len(), 1);
    assert_eq!(paragraphs[0], "<b>軍營村</b>是<a href=\"/wiki/%E9%9D%9E%E6%B4%B2\" title=\"非洲\">非洲</a>東部島國<a href=\"/wiki/%E6%AF%9B%E9%87%8C%E8%A3%98%E6%96%AF\" class=\"mw-redirect\" title=\"毛\r\n里裘斯\">毛里裘斯</a>的城鎮，也是<a href=\"/wiki/%E8%8E%AB%E5%8D%A1%E5%8D%80\" title=\"莫卡區\">莫卡區</a>的首府，位於該國中部，海拔高度330米，該鎮附近大量種植<a href=\"/wiki/%E8%94%97%E7%B3%96\" title=\"蔗糖\">蔗糖</a>，居民主要信奉<a href=\"/wiki/%E4%BC%8A%E6%96%AF%E8%98%AD%E6%95%99\" class=\"mw-redirect\" title=\"伊斯蘭教\">伊斯蘭教</a>，\r\n2009年人口超過30,600。");
}

#[test]
fn string_clean_eng() {
    let result = wikipedia_sentence_sifter::clean_string(r#" In mathematical representation theory, the <b>HT</b> of a pair (<i>g</i>,<i>K</i>) is an algebra with an <a href="/wiki/Approximate_identity" title="Approximate identity">approximate identity</a>, whose approximately unital modules are the same as <i>K</i>-finite representations of the pairs (<i>g</i>,<i>K</i>).   "#);

    assert_eq!(result, "In mathematical representation theory, the HT of a pair (g,K) is an algebra with an approximate identity, whose approximately unital modules are the same as K-finite representations of the pairs (g,K).");
}

#[test]
fn string_clean_zhn() {
    let result = wikipedia_sentence_sifter::clean_string("  <b>軍營村</b>是<a href=\"/wiki/%E9%9D%9E%E6%B4%B2\" title=\"非洲\">非洲</a>東部島國<a class=\"mw-redirect\" href=\"/wiki/%E6%AF%9B%E9%87%8C%E8%A3%98%E6%96%AF\" title=\"毛\r\n里裘斯\">毛里裘斯</a>的城鎮，也是<a href=\"/wiki/%E8%8E%AB%E5%8D%A1%E5%8D%80\" title=\"莫卡區\">莫卡區</a>的首府，位於該國中部，海拔高度330米，該鎮附近大量種植<a href=\"/wiki/%E8%94%97%E7%B3%96\" title=\"蔗糖\">蔗糖</a>.   ");

    assert_eq!(result, "軍營村是非洲東部島國毛里裘斯的城鎮，也是莫卡區的首府，位於該國中部，海拔高度330米，該鎮附近大量種植蔗糖.");
}

#[test]
fn scrape_sentences_eng() {
    let html_string = test_helper_load_file("./tests/html_example_eng.html");
    assert_ne!(html_string, None);
    let sentences = wikipedia_sentence_sifter::get_sentences(".", &html_string.unwrap());
    assert_eq!(sentences.len(), 3);

    assert_eq!(sentences[0], "In mathematical representation theory, the HT of a pair (g,K) is an algebra with an approximate identity, whose approximately unital modules are the same as K-finite representations of the pairs (g,K).");

    assert_eq!(
        sentences[1],
        "Here K is a compact subgroup of a Lie group with Lie algebra g."
    );

    assert_eq!(
        sentences[2],
        "The Hecke algebra of a pair (g,K) is the algebra of K-finite distributions on G with support in K, with the product given by convolution."
    );
}

#[test]
fn scrape_sentences_zhn() {
    let html_string = test_helper_load_file("./tests/html_example_zhn.html");
    assert_ne!(html_string, None);
    let sentences = wikipedia_sentence_sifter::get_sentences("。", &html_string.unwrap());
    assert_eq!(sentences.len(), 1);

    assert_eq!(sentences[0], "軍營村是非洲東部島國毛里裘斯的城鎮，也是莫卡區的首府，位於該國中部，海拔高度330米，該鎮附近大量種植蔗糖，居民主要信奉伊斯蘭教，2009年人口超過30,600。");
}

#[test]
fn choose_sentence_zhn() {
    let sentences = vec![
        "軍營村是非洲東部島國毛里裘斯的城鎮。".to_string(),
        "也是莫卡區的首府。".to_string(),
        "海拔高度330米。".to_string(),
    ];

    assert_eq!(wikipedia_sentence_sifter::find_usage("bogus", &sentences), None);

    assert_eq!(
        wikipedia_sentence_sifter::find_usage("非", &sentences),
        Some("軍營村是非洲東部島國毛里裘斯的城鎮。".to_string())
    );

    assert_eq!(
        wikipedia_sentence_sifter::find_usage("米", &sentences),
        Some("海拔高度330米。".to_string())
    );
}

#[test]
fn choose_sentence_eng() {
    let sentences = vec!["In mathematical representation theory.".to_string(), "The HT of a pair (g,K) is an algebra with an approximate identity, whose approximately unital modules are.".to_string(), "Here K is a compact subgroup of a Lie group with Lie algebra g.".to_string()];

    assert_eq!(wikipedia_sentence_sifter::find_usage("bogus", &sentences), None);

    assert_eq!(
        wikipedia_sentence_sifter::find_usage("here", &sentences),
        Some("Here K is a compact subgroup of a Lie group with Lie algebra g.".to_string())
    );

    assert_eq!(
        wikipedia_sentence_sifter::find_usage("identity", &sentences),
        Some("The HT of a pair (g,K) is an algebra with an approximate identity, whose approximately unital modules are.".to_string())
    );
}
